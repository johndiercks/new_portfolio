<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://kit.fontawesome.com/20d3b3b5c8.js"></script>
    <title>John Diercks Portfolio</title>
</head>
<body>

    <svg class="background background__intro" xmlns="http://www.w3.org/2000/svg" width="1100" height="1673.021" viewBox="0 0 1923 1673.021">
        <path id="Path_1" data-name="Path 1" d="M1918,1595.347V0H-4V1595.347s643.331-172.47,980.979,0S1918,1595.347,1918,1595.347Z" transform="translate(4.5 0.5)" fill="#0A0A0A" stroke="#707070" stroke-width="1"/>
    </svg>

    <?php require('php/public/nav.php'); ?>

    <?php require('php/public/intro.php'); ?>

    <?php require('php/public/skills.php'); ?>

    <svg class="background background__portfolio" xmlns="http://www.w3.org/2000/svg" height="1903.055" viewBox="0 0 826.117 2903.055">
        <path id="Path_2" data-name="Path 2" d="M1920,2799.5s-783.547,351.154-820.5,665.3,157.071,452.733,101.634,859.269,715.112,1376.287,715.112,1376.287Z" transform="translate(-1094.384 -2798.728)" fill="#0a0a0a" stroke="#707070" stroke-width="1"/>
    </svg>

    <?php require('php/public/projects.php'); ?>

    <?php require('php/public/contact.php'); ?>

    <?php require('php/public/footer.php'); ?>

    <script src="../node_modules/typed.js/lib/typed.min.js"></script>
    <script src="../node_modules/chaffle/chaffle.min.js"></script>
    <script src="js/App.js" type="module"></script>
    <script src="js/Vendor.js" type="module"></script>
</body>
</html>