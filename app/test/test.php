<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../css/styles.css">
  <!-- <script src="https://npmcdn.com/chaffle/chaffle.min.js"></script> -->
  <title>Document</title>
</head>
<body>
<footer class="main_footer">
    <h5 class="copyright">johndiercks.biz © <?= date("Y");?></h5>
    <div class="social_media">
    
        <a href="https://www.facebook.com" class="social-media--facebook">
            facebook
        </a>
        <a href="https://www.linkedin.com/in/john-diercks-b66986108/" class="social-media--linkedin">
            linkedin
        </a>
        <a href="https://bitbucket.org/johndiercks/profile/projects" class="social-media--mail">
            bitbucket
        </a>
    
    </div>
</footer>
  <script src="../js/App.js" type="module"></script>
  <script src="../../node_modules/chaffle/chaffle.min.js"></script>
</body>
</html>