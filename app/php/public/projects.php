<h1 class="projects-title">Projects</h1>

<div class="projects grid">

    <img src="images/stews.png" alt="" class="projects-img projects-img__stews grid-item grid-item__projects">

    <img src="images/clear-view-escapes.png" alt="" class="projects-img projects-img__clear-view grid-item grid-item__projects">
    
    <img src="images/dmaccportfolioday.png" alt="" class="projects-img projects-img__dmacc grid-item grid-item__projects">

</div>