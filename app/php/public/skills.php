<div class="skills grid">

    <div class="container grid-item">
        <h2 class="skills-title">Front-End</h2>
        <p>HTML</p>
        <p>CSS3</p>
        <p>SASS</p>
        <p>Bootstrap</p>
        <p>Javascript</p>
        <p>jQuery</p>
        <p>Bulma</p>
    </div>

    <div class="container grid-item">
        <h2 class="skills-title">Back-End</h2>
        <p>PHP</p>
        <p>SQL</p>
        <p>Node.js(currently learning)</p>
        <p>Python(currently learning)</p>
    </div>

    <div class="container grid-item">
        <h2 class="skills-title">Tools</h2>
        <p>Adobe XD</p>
        <p>NPM</p>
        <p>Gulp</p>
        <p>Bitbucket or Git</p>
        <p>Babel</p>
        <p>Webpack or Parcel</p>
    </div>

</div>