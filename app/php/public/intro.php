<section class="main">
    <h1 class="title-name">John Diercks</h1>
    <div class="typing">
        <div id="strings">
            <h3 class="title-job">~ Web Developer ~</h3>
            <h3 class="title-job">~ Web Designer ~</h3>
            <h3 class="title-job">~ Please have a look around ~</h3>
        </div>
        <span id="typed"></span>
    </div>
    <p class="title-saying">"Building websites that Build your buisiness."</p>
</section>