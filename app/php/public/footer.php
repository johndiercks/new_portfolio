<footer class="main_footer">
    <h5 class="copyright">johndiercks.biz © <?= date("Y");?></h5>
    <div class="social_media">
    
    <a href="https://www.linkedin.com/in/john-diercks-b66986108/" class="social-media--linkedin">
        <i class="fab fa-linkedin"></i>
    </a>
    <a href="https://bitbucket.org/johndiercks/profile/projects" class="social-media--mail">
        <i class="fab fa-github"></i>
    </a>
    <a href="https://www.hackerrank.com/jdpivotjohn?hr_r=1" class="social-media--mail">
        <i class="fab fa-hackerrank"></i>
    </a>
    
    </div>
</footer>