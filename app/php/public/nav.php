<nav>
    <div id="menuToggle">

        <input type="checkbox" />

        <span></span>
        <span></span>
        <span></span>

        <ul id="menu">
            <li class="links">
                <a href="about.php" class="about_link">About</a>
            </li>
            <li class="links">
                <a href="images/web_dev_resume.pdf" class="resume_link">Resume</a>
            </li>
            <li class="links">
                <a href="index.php" class="home_link-logo">JD</a>
            </li>
            <li class="links">
                <a href="portfolio.php" class="portfolio_link">Portfolio</a>
            </li>
            <li class="links">
                <a href="contact.php" class="contact_link">Contact</a>
            </li>
        </ul>
    </div>
    <ul id="menuDesktop">
        <li class="links">
            <a href="about.php" class="about_link links_link" data-chaffle="en">About</a>
        </li>
        <li class="links">
            <a href="images/web_dev_resume.pdf" class="resume_link links_link" data-chaffle="en">Resume</a>
        </li>
        <li class="links">
            <a href="index.php" class="home_link-logo links_link" data-chaffle="en">JD</a>
        </li>
        <li class="links">
            <a href="portfolio.php" class="portfolio_link links_link" data-chaffle="en">Portfolio</a>
        </li>
        <li class="links">
            <a href="contact.php" class="contact_link links_link" data-chaffle="en">Contact</a>
        </li>
    </ul>
</nav>